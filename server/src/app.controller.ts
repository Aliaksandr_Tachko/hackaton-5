import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  Request,
  Res,
  Session,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import {
  randomBase64URLBuffer,
  generateServerMakeCredRequest,
  verifyAuthenticatorAttestationResponse,
  verifyAuthenticatorAssertionResponse,
  generateServerGetAssertion,
} from './helpers/utils';
import base64url from 'base64url';
import { v4 as uuidv4 } from 'uuid';
import { createUser, getUserData, db } from './db/db';

@ApiTags('WebAuth')
@Controller('webauthn')
export class AppController {
  @Post('register')
  @HttpCode(200)
  @ApiOperation({ description: 'Register new user' })
  async registerUser(
    @Body() payload: any,
    @Res() response,
    @Request() request,
  ): Promise<any> {
    const { username, name } = payload;

    let user;

    try {
      user = await getUserData(username);
    } catch (e) {}

    if (user) {
      response.json({
        status: 'failed',
        message: `Username ${username} already exists`,
      });

      return response;
    }

    const challengeMakeCred: any = generateServerMakeCredRequest(
      username,
      name,
      randomBase64URLBuffer(),
    );
    challengeMakeCred.status = 'ok';

    request.session.challenge = challengeMakeCred.challenge;
    request.session.username = username;
    request.session.name = name;

    return response.json(challengeMakeCred);
  }

  @Post('response')
  @HttpCode(200)
  @ApiOperation({ description: 'handle challenges' })
  async handleResponse(
    @Body() payload: any,
    @Res() response,
    @Session() session,
  ): Promise<any> {
    const clientData = JSON.parse(
      base64url.decode(payload.response.clientDataJSON),
    );

    /* Check challenge... */
    if (clientData.challenge !== session.challenge) {
      response.json({
        status: 'failed',
        message: "Challenges don't match!",
      });
    }

    let result;
    const oneTimeLoginKey = uuidv4();
    if (payload.response.attestationObject !== undefined) {
      result = verifyAuthenticatorAttestationResponse(payload);

      if (result.verified) {
        await createUser(session.username, {
          authrInfo: result.authrInfo,
          data: { name: session.name, oneTimeLoginKey },
        });
        db[session.username] = {
          oneTimeLoginKey,
        };
      }
    } else if (payload.response.authenticatorData !== undefined) {
      const user = await getUserData(session.username);

      result = verifyAuthenticatorAssertionResponse(payload, [user.authrInfo]);
    } else {
      response.json({
        status: 'failed',
        message: 'Can not determine type of response!',
      });
    }

    if (result.verified) {
      session.loggedIn = true;
      response.json({ status: 'ok', oneTimeLoginKey });
    } else {
      response.json({
        status: 'failed',
        message: 'Can not authenticate signature!',
      });
    }
  }

  @Post('login')
  @HttpCode(200)
  @ApiOperation({ description: 'Login user' })
  async loginUser(
    @Body() payload: any,
    @Res() response,
    @Session() session,
  ): Promise<any> {
    const { username } = payload;

    const user = await getUserData(username);

    if (!user) {
      response.json({
        status: 'failed',
        message: `Username ${username} do not exist!`,
      });
    }

    const getAssertion: any = generateServerGetAssertion([user.authrInfo]);
    getAssertion.status = 'ok';

    session.challenge = getAssertion.challenge;
    session.username = username;

    response.json(getAssertion);
  }

  @Post('login-by-key')
  @HttpCode(200)
  @ApiOperation({ description: 'Login user' })
  async loginUserByKey(
    @Body() payload: any,
    @Res() response,
    @Session() session,
  ): Promise<any> {
    const { username, oneTimeLoginKey } = payload;
    const user = await getUserData(username);

    if (!user || !db[username]) {
      response.json({
        status: 'failed',
        message: `Username ${username} or key do not exist!`,
      });
    }
    if (oneTimeLoginKey) {
      if (!user || !db[username].oneTimeLoginKey) {
        response.json({
          status: 'failed',
          message: `One time key has already been used!`,
        });
      }

      if (
        user &&
        db[username].oneTimeLoginKey &&
        db[username].oneTimeLoginKey === oneTimeLoginKey
      ) {
        session.loggedIn = true;
        db[username].oneTimeLoginKey = null;
        response.json({
          status: 'ok',
        });
      }
    }
    response.json({
      status: 'failed',
      message: `The key is incorrect or smth went wrong!`,
    });
  }

  @Get('isLoggedIn')
  @HttpCode(200)
  @ApiOperation({ description: 'Login check user' })
  async loginCheckUser(@Res() response, @Session() session): Promise<any> {
    if (session.loggedIn) {
      response.json({ status: 'ok' });
    } else {
      response.json({ status: 'failed', message: 'User is not logged in!' });
    }
  }

  @Get('logout')
  @HttpCode(200)
  @ApiOperation({ description: 'logout user' })
  async logout(@Res() response, @Session() session): Promise<any> {
    session.loggedIn = false;
    session.username = undefined;
    response.json({ status: 'ok' });
  }
}
