import * as ethDB from './eth-db';
import { encrypt, IEncryptionResult } from '../utils/crypto';

export const PUBLIC_KEY_PREFIX = '-----BEGIN PUBLIC KEY-----\n';
export const PUBLIC_KEY_SUFFIX = '-----END PUBLIC KEY-----\n';
export const DIVIDER = '#$#';

export const db = {};

export interface IUserData {
  authrInfo: {
    fmt: string;
    publicKey: string;
    counter: number;
    credID: string;
  };
  data: Record<any, any>;
}

export interface IEncryptedUserData {
  authrInfo: {
    fmt: string;
    publicKey: string;
    counter: number;
    credID: string;
  };
  encryptionResult: IEncryptionResult;
}

export const createUser = async (
  username: string,
  userData: IUserData,
): Promise<string> => {
  const encryptionResult = encrypt(
    JSON.stringify(userData.data),
    userData.authrInfo.publicKey,
  );

  const payload = JSON.stringify({
    authrInfo: userData.authrInfo,
    encryptionResult,
  } as IEncryptedUserData);

  console.log(username, payload);

  await ethDB.createUser(username, payload);

  return payload;
};

export const getUserData = async (
  username: string,
): Promise<IEncryptedUserData> => {
  const payload = await ethDB.getUserData(username);
  return JSON.parse(payload);
};

export const database = {};
