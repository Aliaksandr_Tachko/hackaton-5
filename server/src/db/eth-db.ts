import {readFileSync} from 'fs';
const Web3 = require('web3');

// const MAIN_PROVIDER = 'https://quaint-late-pool.discover.quiknode.pro/b15d8c4cd54d45900ba6bd30a81f2a85680f30dd/';
// const MAIN_CONTRACT_ABI = JSON.parse(readFileSync('./contract-abi.json').toString());
// const MAIN_CONTRACT_ADDRESS = '0x591a2Cafb8b12b5601E299de35615ef6FFf6F18f';

const TEST_PROVIDER = 'https://soft-solemn-grass.ethereum-sepolia.discover.quiknode.pro/1b251fd6915ac3a77bd33b9ab430ebafdc437dcf/';
const TEST_CONTRACT_ABI = JSON.parse(readFileSync('./src/contracts/contract-abi.json').toString());
const TEST_CONTRACT_ADDRESS = '0x591a2Cafb8b12b5601E299de35615ef6FFf6F18f';

const web3Provider = new Web3.providers.HttpProvider(TEST_PROVIDER);
const web3 = new Web3(web3Provider)
const contractInstance = new web3.eth.Contract(TEST_CONTRACT_ABI, TEST_CONTRACT_ADDRESS);

const adminAccount = web3.eth.accounts.privateKeyToAccount('2d7630e41bc23f82eb9ae572a816517cb732fb38f1132aaa0920673edf452658');

export const createUser = async (username: string, userData: string): Promise<void> => {
    await web3.eth.accounts.wallet.add(adminAccount);
    await contractInstance.methods.createUser(username, userData)
      .send({
          from: adminAccount.address,
          gas: 10000000,
          gasPrice: 10
      });
}

export const getUserData = async (username: string): Promise<string> => {
    return await contractInstance.methods.getUserData(username).call({_username: username});
}
