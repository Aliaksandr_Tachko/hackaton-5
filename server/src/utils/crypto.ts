// import * as crypto from 'crypto';
//
// export const encrypt = (text: string, publicKey: string): string => {
//     return crypto.publicEncrypt(publicKey, Buffer.from(text, 'utf8')).toString('hex');
// }
//
// export const decrypt = (encryptedText: string, privateKey: string): string => {
//     return crypto.privateDecrypt(privateKey, Buffer.from(encryptedText, 'hex')).toString('utf8');
// }
//
// export const generateKeys = () => {
//     return crypto.generateKeyPairSync('rsa', {
//         modulusLength: 700,
//         publicKeyEncoding: {
//             type: 'spki',
//             format: 'pem'
//         },
//         privateKeyEncoding: {
//             type: 'pkcs8',
//             format: 'pem',
//         }
//     });
// }



import * as crypto from 'crypto';

const algorithm = 'aes-256-ctr';

export interface IEncryptionResult {
    iv: string;
    encryptedText: string
}

export const encrypt = (text: string, publicKey: string): IEncryptionResult => {
    const iv = crypto.randomBytes(16)
    const secretKey = Buffer.from(publicKey, 'utf8').toString('hex').slice(0, 32);

    const cipher = crypto.createCipheriv(algorithm, secretKey, iv)

    const encrypted = Buffer.concat([cipher.update(text), cipher.final()])

    return {
        iv: iv.toString('hex'),
        encryptedText: encrypted.toString('hex')
    }
}

export const decrypt = (encryptionResult: IEncryptionResult, publicKey: string) => {
    const iv = encryptionResult.iv;
    const secretKey = Buffer.from(publicKey, 'utf8').toString('hex').slice(0, 32);

    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(iv, 'hex'))

    const decrypted = Buffer.concat(
      [decipher.update(Buffer.from(encryptionResult.encryptedText, 'hex')), decipher.final()]
    );

    return decrypted.toString('utf8');
}
