// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title WebAuthNHackathon
 *
 * @dev Implementation of the basic distributed db to save webauthn data.
 */
contract Storage {

  mapping(string => string) users;

  /**
  * @dev Creates new user.
  * @param _username The username used as a key to save data.
  * @param _userData The user data to persist in blockchain.
  */
  function createUser(string calldata _username, string calldata _userData) public {
    bytes memory userDataBytes = bytes(users[_username]);
    require(userDataBytes.length == 0, "User already exists");
    users[_username] = _userData;
  }

  /**
  * @dev Gets the data of the specified _username.
  * @param _username The username to get the data.
  * @return A string representing the user data.
  */
  function getUserData(string calldata _username) public view returns (string memory) {
    bytes memory userDataBytes = bytes(users[_username]);
    require(userDataBytes.length != 0, "User not exists");
    return users[_username];
  }

}
