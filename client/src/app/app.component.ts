import {Component, OnInit} from '@angular/core';
import {AuthService} from "./services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public isLoggedIn$ = this.authService.isLoggedIn;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }

  public ngOnInit(): void {
    this.checkLogin();
  }

  private checkLogin(): void  {
    this.authService.checkIfLoggedIn().subscribe((res) => {
      if(res) {
        this.authService.isLoggedIn.next(true);
      }
    })
  }

  public handleLogout(): void  {
    this.authService.logout().subscribe(() => {
      this.authService.isLoggedIn.next(false);
      localStorage.removeItem('username');
      this.router.navigate(['login'])
    });
  }
}
