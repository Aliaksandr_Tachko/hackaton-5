import {Buffer} from 'buffer';

export const convertFromBase64ToArrayBuffer = (base64String: string): ArrayBuffer => {
  const plainText = Buffer.from(base64String, 'base64').toString('binary');
  return Uint8Array.from(plainText, c => c.charCodeAt(0))
}

export const convertArrayBufferToBase64 = (arrayBuffer: ArrayBuffer): string => {
  const buffer = Buffer.from(arrayBuffer);
  return buffer.toString('base64');
}

export const preformatMakeCredReq = (makeCredReq: any) => {
  makeCredReq.challenge = convertFromBase64ToArrayBuffer(makeCredReq.challenge);
  makeCredReq.user.id = convertFromBase64ToArrayBuffer(makeCredReq.user.id);
  return makeCredReq
}

// @ts-ignore
export const publicKeyCredentialToJSON = (pubKeyCred: any) => {
  if (pubKeyCred instanceof Array) {
    let arr = [];
    for (let i of pubKeyCred)
      arr.push(publicKeyCredentialToJSON(i));

    return arr
  }

  if (pubKeyCred instanceof ArrayBuffer) {
    return convertArrayBufferToBase64(pubKeyCred);
  }

  if (pubKeyCred instanceof Object) {
    let obj = {};

    for (let key in pubKeyCred) {
      // @ts-ignore
      obj[key] = publicKeyCredentialToJSON(pubKeyCred[key]) as any
    }

    return obj
  }

  return pubKeyCred
}

export const preformatGetAssertReq = (getAssert: any) => {
  getAssert.challenge = convertFromBase64ToArrayBuffer(getAssert.challenge);

  for(let allowCred of getAssert.allowCredentials) {
    allowCred.id = convertFromBase64ToArrayBuffer(allowCred.id);
  }

  return getAssert
}
