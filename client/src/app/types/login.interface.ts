export interface ILoginDetails {
  name: string;
  username: string;
}
