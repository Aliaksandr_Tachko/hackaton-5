export interface IBasicResponse {
  status: string;
  message?: string;
  oneTimeLoginKey?: string;
}
