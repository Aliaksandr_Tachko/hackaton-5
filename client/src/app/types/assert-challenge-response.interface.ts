import {IBasicResponse} from "./basic-response.interface";

export interface IAssertChallengeResponse extends IBasicResponse{
  "challenge": string;
  "allowCredentials": {
    "type": string;
    "id": string;
    "transports": string[];
  }[],
}
