import {IBasicResponse} from "./basic-response.interface";

export interface ICredentialsResponse extends IBasicResponse{
  challenge: string,
  rp: {
    name: string,
  },
  user: {
    id: string,
    name: string,
    displayName: string,
  },
  attestation: string,
  pubKeyCredParams: [
    {
      type: string,
      alg: number;
    }
  ],
}
