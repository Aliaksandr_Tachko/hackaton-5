import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from "./material/material.module";
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginContainerComponent } from './containers/login-container/login-container.component';
import { RegisterContainerComponent } from './containers/register-container/register-container.component';
import { LoginByKeyComponent } from './components/login-by-key/login-by-key.component';
import { LoginByKeyContainerComponent } from './containers/login-by-key-container/login-by-key-container.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    LoginContainerComponent,
    RegisterContainerComponent,
    LoginByKeyComponent,
    LoginByKeyContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  // entryComponents: [LoginComponent, RegisterComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private injector: Injector) {
  //   const loginComponent = createCustomElement(LoginComponent, {injector});
  //   const registerComponent = createCustomElement(RegisterComponent, {injector});
  //   customElements.define('ui-login', loginComponent);
  //   customElements.define('ui-register', registerComponent);
  // }
  //
  // ngDoBootstrap() {}
}
