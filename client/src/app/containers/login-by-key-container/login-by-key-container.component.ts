import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-by-key-container',
  templateUrl: './login-by-key-container.component.html',
  styleUrls: ['./login-by-key-container.component.scss']
})
export class LoginByKeyContainerComponent {
  constructor(
    private router: Router,
  ) {}

  public handleLoggedIn(): void {
    this.router.navigate(['dashboard']);
  }
}
