import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginByKeyContainerComponent } from './login-by-key-container.component';

describe('LoginByKeyContainerComponent', () => {
  let component: LoginByKeyContainerComponent;
  let fixture: ComponentFixture<LoginByKeyContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginByKeyContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginByKeyContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
