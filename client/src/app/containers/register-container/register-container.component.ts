import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-register-container',
  templateUrl: './register-container.component.html',
  styleUrls: ['./register-container.component.scss']
})
export class RegisterContainerComponent {
  constructor(
    private router: Router,
  ) {}

  public handleRegistered(): void {
    this.router.navigate(['dashboard']);
  }
}
