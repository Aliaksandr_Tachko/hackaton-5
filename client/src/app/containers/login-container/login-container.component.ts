import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-container',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.scss']
})
export class LoginContainerComponent {
  constructor(
    private router: Router,
  ) {}

  public handleLoggedIn(): void {
    this.router.navigate(['dashboard']);
  }
}
