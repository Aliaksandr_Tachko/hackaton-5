import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root',
})
export class ToastService {

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar
  ) {
  }

  public showMessage(message: string): void {
    this.snackBar.open(message, '', {
      duration: 3000,
      panelClass: ['success-snackbar']
    });
  }

  public showErrorMessage(message: string): void {
    this.snackBar.open(message, '', {
      duration: 3000,
      panelClass: 'error-snackbar',
    });
  }
}
