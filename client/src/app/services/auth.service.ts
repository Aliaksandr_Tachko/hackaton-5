import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, map, Observable, tap } from 'rxjs';

import { ILoginDetails } from '../types/login.interface';
import { ICredentialsResponse } from '../types/credential.interface';
import { IBasicResponse } from '../types/basic-response.interface';
import { IAssertChallengeResponse } from '../types/assert-challenge-response.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly url = 'http://localhost:3000/webauthn';

  public isLoggedIn = new BehaviorSubject(false);
  public oneTimeKey: string | undefined = '';

  constructor(private httpClient: HttpClient) {}

  public getMakeCredentialsChallenge(obj: ILoginDetails): Observable<ICredentialsResponse> {
    return this.httpClient
      .post<ICredentialsResponse>(`${this.url}/register`, JSON.stringify(obj), {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true,
      })
      .pipe(
        tap((response) => {
          if (response.status !== 'ok') {
            throw new Error(response.message);
          }
        }),
      );
  }

  public sendWebAuthnResponse(obj: any): Observable<IBasicResponse> {
    return this.httpClient
      .post<any>(`${this.url}/response`, JSON.stringify(obj), {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true,
      })
      .pipe(
        tap((response) => {
          if (response.status !== 'ok') {
            throw new Error(response.message);
          }
        }),
      );
  }

  public getAssertionChallenge(obj: any): Observable<IAssertChallengeResponse> {
    return this.httpClient
      .post<any>(`${this.url}/login`, JSON.stringify(obj), {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true,
      })
      .pipe(
        tap((response) => {
          if (response.status !== 'ok') {
            throw new Error(response.message);
          }
        }),
      );
  }

  public checkIfLoggedIn(): Observable<boolean> {
    return this.httpClient
      .get<IBasicResponse>(`${this.url}/isLoggedIn`, {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true,
      })
      .pipe(map((response: IBasicResponse) => response.status === 'ok'));
  }

  public logout(): Observable<IBasicResponse> {
    return this.httpClient.get<IBasicResponse>(`${this.url}/logout`, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true,
    });
  }

  public sendOneTimeLogin(obj: any): Observable<IBasicResponse> {
    return this.httpClient
      .post<any>(`${this.url}/login-by-key`, JSON.stringify(obj), {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true,
      })
      .pipe(
        tap((response) => {
          if (response.status !== 'ok') {
            throw new Error(response.message);
          }
        }),
      );
  }
}
