import {inject, NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {tap} from "rxjs";

import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {AuthService} from "./services/auth.service";
import {LoginContainerComponent} from "./containers/login-container/login-container.component";
import {RegisterContainerComponent} from "./containers/register-container/register-container.component";
import {LoginByKeyContainerComponent} from "./containers/login-by-key-container/login-by-key-container.component";

export const canActivateGuard = () => {
  const router = inject(Router);
  const service = inject(AuthService)
  return service.checkIfLoggedIn().pipe(
    tap((value) => {
        return !value ? router.navigate(['/login']) : true
      }
    ))
}

const routes: Routes = [
  {
    path: 'login', component: LoginContainerComponent,
  },
  {
    path: 'login-by-key', component: LoginByKeyContainerComponent,
  },
  {
    path: 'register', component: RegisterContainerComponent,
  },
  {
    path: 'dashboard', component: DashboardComponent, canActivate: [canActivateGuard]
  },
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
