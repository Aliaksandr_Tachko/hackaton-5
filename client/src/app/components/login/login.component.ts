import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {from, switchMap} from "rxjs";

import {AuthService} from "../../services/auth.service";
import {preformatGetAssertReq, publicKeyCredentialToJSON} from "../../helpers/auth.helper";
import {ToastService} from "../../services/toast.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public form: FormGroup;
  @Output() public loggedIn = new EventEmitter<void>();

  constructor(
    fb: FormBuilder,
    private authService: AuthService,
    private toastService: ToastService
  ) {
    this.form = fb.group({
      username: ['', Validators.required],
    });
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    const form = this.form.value;
    const {username} = form;

    this.authService.getAssertionChallenge(form)
      .pipe(
        switchMap((response) => {
          let publicKey = preformatGetAssertReq(response);
          return from(navigator.credentials.get({publicKey}));
        }),
        switchMap((response) => {
          let getAssertionResponse = publicKeyCredentialToJSON(response);
          return this.authService.sendWebAuthnResponse(getAssertionResponse);
        })
      )
      .subscribe({
          next: () => {
            this.authService.isLoggedIn.next(true);
            this.toastService.showMessage('Logged in successfully');
            localStorage.setItem('username', username);
            this.loggedIn.next();
          },
          error: error => {
            this.toastService.showErrorMessage(error.message)
          }
        },
      )
  }
}
