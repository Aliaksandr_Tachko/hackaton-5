import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {AuthService} from "../../services/auth.service";
import {ToastService} from "../../services/toast.service";

@Component({
  selector: 'app-login-by-key',
  templateUrl: './login-by-key.component.html',
  styleUrls: ['./login-by-key.component.scss']
})
export class LoginByKeyComponent {
  public form: FormGroup;
  @Output() public loggedIn = new EventEmitter<void>();

  constructor(
    fb: FormBuilder,
    private authService: AuthService,
    private toastService: ToastService
  ) {
    this.form = fb.group({
      username: ['', Validators.required],
      oneTimeLoginKey: ['', Validators.required]
    });
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    const form = this.form.value;
    const {username} = form;
    this.authService.sendOneTimeLogin(form)
      .subscribe({
          next: () => {
            this.authService.isLoggedIn.next(true);
            this.toastService.showMessage('Logged in successfully');
            localStorage.setItem('username', username);
            this.loggedIn.emit();
          },
          error: error => {
            this.toastService.showErrorMessage(error.message)
          }
        },
      )
  }
}
