import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {from, switchMap} from 'rxjs';

import {AuthService} from '../../services/auth.service';
import {preformatMakeCredReq, publicKeyCredentialToJSON} from '../../helpers/auth.helper';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  public form: FormGroup;
  public loading = false;

  @Output() public registered = new EventEmitter<void>();

  constructor(fb: FormBuilder, private authService: AuthService, private toastService: ToastService) {
    this.form = fb.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
    });
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const value = this.form.value;
    const {username} = value;
    this.authService
      .getMakeCredentialsChallenge(value)
      .pipe(
        switchMap((response) => {
          let publicKey = preformatMakeCredReq(response);
          return from(navigator.credentials.create({publicKey}));
        }),
        switchMap((response) => {
          let makeCredResponse = publicKeyCredentialToJSON(response);
          return this.authService.sendWebAuthnResponse(makeCredResponse);
        }),
      )
      .subscribe({
        next: (response) => {
          this.loading = false;
          this.authService.oneTimeKey = response.oneTimeLoginKey;
          this.authService.isLoggedIn.next(true);
          this.toastService.showMessage('Registered successfully');
          localStorage.setItem('username', username);
          this.registered.emit();
        },
        error: (error) => {
          this.loading = false;
          this.toastService.showErrorMessage(error.message);
        },
      });
  }
}
