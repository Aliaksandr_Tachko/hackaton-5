import { Component } from '@angular/core';
import { from, switchMap } from 'rxjs';

import { preformatGetAssertReq, publicKeyCredentialToJSON } from '../../helpers/auth.helper';
import { AuthService } from '../../services/auth.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  public showCat = false;
  public oneTimeCode;
  public showOneTimeCode = false;

  constructor(private authService: AuthService, private toastService: ToastService) {
    this.oneTimeCode = authService.oneTimeKey;
    this.showOneTimeCode = !!authService.oneTimeKey;
  }

  public closeKey(): void {
    this.showOneTimeCode = false;
  }

  public verify(): void {
    const username = localStorage.getItem('username');
    this.authService
      .getAssertionChallenge({ username })
      .pipe(
        switchMap((response) => {
          let publicKey = preformatGetAssertReq(response);
          return from(navigator.credentials.get({ publicKey }));
        }),
        switchMap((response) => {
          let getAssertionResponse = publicKeyCredentialToJSON(response);
          return this.authService.sendWebAuthnResponse(getAssertionResponse);
        }),
      )
      .subscribe({
        next: () => {
          this.showCat = true;
        },
        error: (error) => {
          this.toastService.showErrorMessage(error.message);
        },
      });
  }

  public ngOnDestroy(): void {
    this.authService.oneTimeKey = '';
  }
}
