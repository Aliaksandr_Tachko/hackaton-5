import './App.css';
import {useEffect, useRef} from "react";

function App() {
    const loginRef = useRef('');
    const registerRef = useRef('');

    function handleLogin() {
        // login
    }

    function handleRegister() {
        //register
    }

    useEffect(() => {
        loginRef.current.addEventListener('loggedIn', handleLogin);
        registerRef.current.addEventListener('registered', handleRegister);
    }, [])

    return (
        <>
            <div className='card'>
                <ui-login ref={loginRef}></ui-login>
            </div>
            <div className='card'>
                <ui-register ref={registerRef}></ui-register>
            </div>
        </>
    );
}

export default App;
